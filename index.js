// Operators

// Assignment Operators
// Basic Assignment Operator (=)
let assignmentNumber = 8;

// reassigned value to assignmentNumber
assignmentNumber = assignmentNumber + 2;
console.log("Result of addition assignment operator: " + assignmentNumber);

// reassigned value to assignmentNumber (shortcut)
// Addition Assignment Operator (+=)
assignmentNumber += 2;
console.log("Result of addition assignment operator: " + assignmentNumber);

// Subt/Multp/Divi Assignment Operator (-=, *=, /=, %=)

// Arithmetic Operators +,/,*,/,%(Modulo)
let x = 1397;
let y = 7831;

let sum = x + y;

console.log("Result of addition operator: " + sum);

let difference = x - y;
console.log("Result of addition operator: " + difference);

let product = x * y;
console.log("Result of addition operator: " + product);

let quotient = x / y;
console.log("Result of addition operator: " + quotient);

// Modulo (%) returns the remainder
let modulus = x % y;
console.log("Result of addition operator: " + modulus);

// Multiple Operators and Parentheses

let mdas = 1 +2 - 3 * 4 / 5;
console.log("Result of mdas operation " + mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of pemdas operation " + pemdas);

// Increment and Decrement
// Adding or subtracting values of 1 and reassigning..
let z = 1;

// Pre-increment (adds 1 to current value)
let increment = ++z;
console.log("Result of pre-increment: " + increment);
console.log("Result of pre-incrememntL " + z);

// Post-increment
// value of "z" is returned and stored in the variable "increment" then the value of "z" is increased by one
increment = z++;
console.log("Result of post-increment: " + increment);
console.log("Result of post-increment: " + z);

// pre-decrement (subtracts 1 to current value)
let decrement = --z;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);

// value of "z" is returned and stored in the variable "increment" then the value of "z" is decreased by one
decrement = z--;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);

// Type Coercion
// automatic/implicit conversion of values from one data type to another

let numA = "10";
let numB = 12;

// nagmerge si numA and numB (typeof) resulting a new type of data(ex.str)
let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

// Boolean true/false = 1/0
let numE = true + 1
console.log(numE);

let numF = false + 1
console.log(numF);

// Comparison Operators
// will check if we have items that are equivalent or not

let juan = "juan";

// Equality Operator (==)
// checks whether the operants are equal of has the same content
// returns a boolean value (is _ equal to _?)

console.log(1 == 1);
console.log(1 == 2);
console.log(1 == '1');
console.log(0 == false);
// compares two str that are the same
console.log('juan' == 'juan');
// compares a str with the variable 'juan'
console.log('juan' == juan);

// Inequality operator
// checks whether the operants are not equal/have diff content (is _ not equal to _ ?)
// console.log(1 != 1);
// console.log(1 != 2);
// console.log(1 != '1');
// console.log(0 != false);
// console.log('juan' != 'juan');
// console.log('juan' != juan);

// Strict Equality Operator (===) (pareho dapat pati data type)
console.log("With Strict Equality Operator");
console.log(1 === 1);
console.log(1 === 2);
console.log(1 === '1');
console.log(0 === false);
console.log('juan' === 'juan');
console.log('juan' === juan);

// Strict Inequality Operator (!==) (pareho dapat pati data type)
console.log("With Strict Inequality Operator");
console.log(1 !== 1);
console.log(1 !== 2);
console.log(1 !== '1');
console.log(0 !== false);
console.log('juan' !== 'juan');
console.log('juan' !== juan);

// Logical Operators
let isLegalAge = true;
let isRegistered = false;

// Logical AND Operator (&&)
// Returns true if all operants are true
let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of logical AND operator: " + allRequirementsMet);

// Logical OR operator (||)
// Return true if one of operants are true
let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of logical OR operator: " + someRequirementsMet);

// Logical NOT operator (!)
// Returns the opposite value (negates the value)
let someRequirementsNotMet = !isRegistered;
console.log("Result of logical NOT operator: " + someRequirementsNotMet);

/*Relational Operators
	< - Less than
	> - Greater Than
	<= - Less than or equal to
	>= - Greater than or equal to

	let a = 5
	let relate = a <= 5;
	console.log(relate);
*/

// Selection Control Structures
// if, else if and else statement

let numG = -1

// if Statement - executes a statmnt if a specified condition is true
/* Syntax:
	if(condition){
	statement/s;
	}
*/
if(numG < 0){
	console.log("Hello");
}

let numH = 1
// else if Clause - executes statement if previous conditions are false and if the specified condition is true

if(numG > 0){
	console.log("Hello");
}
else if (numH == 0){
	console.log("World");
}
else if (numH > 0){
	console.log("Solar System");
}

// else Statement - executes a statement if all other coonditions are false
if(numG > 0){
	console.log("Hello");
}
else if (numH == 0){
	console.log("World");
}
else{
	console.log("Try Again");	
}

/*
	Department A = 1 - 3
	Department B = 4 - 6
	Department C = 7 - 9
*/

let dept = 4;

if(dept >= 1 && dept <=3){
	console.log("You're in Department A");
}
else if(dept >= 4 && dept <=6){
	console.log("You're in Department B");
}
else if(dept >= 7 && dept <=9){
	console.log("You're in Department B");
}
else{
	console.log("Department does not exist");
}

let message = "No Message";
console.log(message);

function determineTyphoonIntensity(windSpeed){
	if(windSpeed < 30){
		return "Not a typhoon yet.";
	}
	else if(windSpeed < 61){
		return "Tropical depression detected.";
	}
	else if(windSpeed >= 62 && windSpeed <=88){
		return "Tropical storm detected.";
	}
	else if(windSpeed >= 89 && windSpeed <=117){
		return "Severe tropical storm detected.";
	}
	else{
		return "Typhoon detected.";
	}
}

// Return the str to the variable "message" that invoked it
message = determineTyphoonIntensity(66);
console.log(message);

// Truthy and Falsy
// In JS a "truthy" = value considered true when encountered in Boolean context (everything is true unless specified 1/0)
/*Falsy Values
	1. false
	2. 0
	3. -0
	4. " "
	5. null
	6. undefined
	7. NaN
*/

if (true){
	console.log('Truthy');
}

if (1){
	console.log('Truthy');
}

// Falsy example
if (false){
	console.log('Falsy');
}

if (0){
	console.log('Falsy');
}

if (undefined){
	console.log('Falsy');
}

// Single statement execution
// Conditional Ternary Operator
/* Syntax
	(expression) ? ifTrue : ifFalse;
*/
let ternaryResult = (1 < 18) ? true : false
console.log("Result of Ternary Operator: " + ternaryResult);

let name;

function isOfLegalAge() {
	name = 'John';
	return 'You are of the legal age limit'
}
function isUnderAge(){
	name = 'Jane';
	return 'You are under the age limit'
}

// parseInt(para tumanggap ng number yung prompt kase naka "str")
let age = parseInt(prompt("What is your age?"));
console.log(age);

// ang magiging value ng legalAge is everything next to (=) [ex.isUnderlegalAge (everything na laman ng isUnderLegalAge sa taas na function)]
let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
console.log("Result of Ternary Operator Functions: " + legalAge + "," + name);

// Switch statement
/*  Syntax
	switch (expression) {
		case value1:
			statement/s;
			break;
		case value2:
			statement/s;
			break;
		case valueN:
			statement/s;
			break;
		default:
			statement/s;

	}
	** "value" dapat equivalent sa "expression" to execute
*/

let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day);

switch (day){
	case 'monday':
		console.log("The color of the day is red");
		break;
	case 'tuesday':
			console.log("The color of the day is orange");
			break;
	case 'wednesday':
			console.log("The color of the day is yellow");
			break;
	case 'thursday':
			console.log("The color of the day is green");
			break;
	case 'friday':
			console.log("The color of the day is blue");
			break;
	case 'saturday':
			console.log("The color of the day is indigo");
			break;
	case 'sunday':
			console.log("The color of the day is violet");
			break;
	default:
		console.log("Please input a valid day");

// Try-Catch-Finally Statement
// try-catch - used for error handling
// Finally block - used to specify a response/action to handle/resolve errors

function showIntensityAlert(windSpeed){
	try{
		alert(determineTyphoonIntensity(windSpeed));
	}
	catch (error){
		console.log(typeof error);
	}
	finally {
		alert('Intensity updates will show new alert.');
	}
}

showIntensityAlert(56);




}
